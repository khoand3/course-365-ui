'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
  description: 'This DB includes all courses in system',
  courses: [
    {
      id: 1,
      courseCode: 'FE_WEB_ANGULAR_101',
      courseName: 'How to easily create a website with Angular',
      price: 750,
      discountPrice: 600,
      duration: '3h 56m',
      level: 'Beginner',
      coverImage: 'images/courses/course-angular.jpg',
      teacherName: 'Morris Mccoy',
      teacherPhoto: 'images/teacher/morris_mccoy.jpg',
      isPopular: false,
      isTrending: true,
    },
    {
      id: 2,
      courseCode: 'BE_WEB_PYTHON_301',
      courseName: 'The Python Course: build web application',
      price: 1050,
      discountPrice: 900,
      duration: '4h 30m',
      level: 'Advanced',
      coverImage: 'images/courses/course-python.jpg',
      teacherName: 'Claire Robertson',
      teacherPhoto: 'images/teacher/claire_robertson.jpg',
      isPopular: false,
      isTrending: true,
    },
    {
      id: 5,
      courseCode: 'FE_WEB_GRAPHQL_104',
      courseName: 'GraphQL: introduction to graphQL for beginners',
      price: 850,
      discountPrice: 650,
      duration: '2h 15m',
      level: 'Intermediate',
      coverImage: 'images/courses/course-graphql.jpg',
      teacherName: 'Ted Hawkins',
      teacherPhoto: 'images/teacher/ted_hawkins.jpg',
      isPopular: true,
      isTrending: false,
    },
    {
      id: 6,
      courseCode: 'FE_WEB_JS_210',
      courseName: 'Getting Started with JavaScript',
      price: 550,
      discountPrice: 300,
      duration: '3h 34m',
      level: 'Beginner',
      coverImage: 'images/courses/course-javascript.jpg',
      teacherName: 'Ted Hawkins',
      teacherPhoto: 'images/teacher/ted_hawkins.jpg',
      isPopular: true,
      isTrending: true,
    },
    {
      id: 8,
      courseCode: 'FE_WEB_CSS_111',
      courseName: 'CSS: ultimate CSS course from beginner to advanced',
      price: 750,
      discountPrice: 600,
      duration: '3h 56m',
      level: 'Beginner',
      coverImage: 'images/courses/course-css.jpg',
      teacherName: 'Juanita Bell',
      teacherPhoto: 'images/teacher/juanita_bell.jpg',
      isPopular: true,
      isTrending: true,
    },
    {
      id: 14,
      courseCode: 'FE_WEB_WORDPRESS_111',
      courseName: 'Complete Wordpress themes & plugins',
      price: 1050,
      discountPrice: 900,
      duration: '4h 30m',
      level: 'Advanced',
      coverImage: 'images/courses/course-wordpress.jpg',
      teacherName: 'Clevaio Simon',
      teacherPhoto: 'images/teacher/clevaio_simon.jpg',
      isPopular: true,
      isTrending: false,
    },
  ],
};
//khai bao bien chua mang du lieu cac khoa hoc
var gCoursesArr = gCoursesDB.courses;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  'use strict';
  //goi ham xu ly load du lieu o vung popular
  loadDataToPopularSection();
  //goi ham xu ly load du lieu o vung trending
  loadDataToTrendingSection();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham xu ly load du lieu o vung popular
function loadDataToPopularSection() {
  'use strict';
  //truy xuat phan tu card-deck chua cac card khoa hoc popular
  var vPopularCardDeck = $('#popular-card-deck');
  //xoa trang vung popular
  vPopularCardDeck.empty();
  for (var bI = 0; bI < gCoursesArr.length; bI++) {
    //kiem tra khoa hoc co popular khong, neu co gan du lieu vao card va append vao card-deck
    if (gCoursesArr[bI].isPopular === true) {
      var bPopularCard = $(`
        <div class="card">
        <img src="./${gCoursesArr[bI].coverImage}" class="card-img-top" alt="${gCoursesArr[bI].courseCode}">
        <div class="card-body">
          <p class="card-title text-primary font-weight-bold">${gCoursesArr[bI].courseName}</p>
          <p class="card-text"><i class="far fa-clock"></i> ${gCoursesArr[bI].duration} ${gCoursesArr[bI].level}</p>
          <p class="card-text"><span class="font-weight-bold">$${gCoursesArr[bI].discountPrice}</span> <del style="opacity: 0.6;">$${gCoursesArr[bI].price}</del></p>
        </div>
        <div class="card-footer d-flex align-items-center justify-content-between">
              <div class="d-flex align-items-center">
                <img src="./${gCoursesArr[bI].teacherPhoto}" alt="${gCoursesArr[bI].teacherName}" class="rounded-circle" width="20%">
                <small class="text-muted ml-2">${gCoursesArr[bI].teacherName}</small>
              </div>
              <i class="far fa-bookmark"></i>
            </div>
          </div>`);
      bPopularCard.appendTo(vPopularCardDeck);
    }
  }
}
//ham xu ly load du lieu o vung treding
function loadDataToTrendingSection() {
  'use strict';
  //truy xuat phan tu card-deck chua cac card khoa hoc trending
  var vTrendingCardDeck = $('#trending-card-deck');
  //xoa trang vung trending
  vTrendingCardDeck.empty();
  for (var bI = 0; bI < gCoursesArr.length; bI++) {
    //kiem tra khoa hoc co trending khong, neu co gan du lieu vao card va append vao card-deck
    if (gCoursesArr[bI].isTrending === true) {
      var bTrendingCard = $(`
          <div class="card">
          <img src="./${gCoursesArr[bI].coverImage}" class="card-img-top" alt="${gCoursesArr[bI].courseCode}">
          <div class="card-body">
            <p class="card-title text-primary font-weight-bold">${gCoursesArr[bI].courseName}</p>
            <p class="card-text"><i class="far fa-clock"></i> ${gCoursesArr[bI].duration} ${gCoursesArr[bI].level}</p>
            <p class="card-text"><span class="font-weight-bold">$${gCoursesArr[bI].discountPrice}</span> <del style="opacity: 0.6;">$${gCoursesArr[bI].price}</del></p>
          </div>
          <div class="card-footer d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                  <img src="./${gCoursesArr[bI].teacherPhoto}" alt="${gCoursesArr[bI].teacherName}" class="rounded-circle" width="20%">
                  <small class="text-muted ml-2">${gCoursesArr[bI].teacherName}</small>
                </div>
                <i class="far fa-bookmark"></i>
              </div>
            </div>`);
      bTrendingCard.appendTo(vTrendingCardDeck);
    }
  }
}
